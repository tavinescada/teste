package modelo;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

public class Emprestimo {
    
    private Date dataI;
    private Date dataF;
    private String stat;
    
    
    //associações entre classes
    private Vector <Livro> registroLivros;
    private Cliente cliente;
    private Vector <Livro> carrinhoEmprestimo;
    
    
    //construtores
    public Emprestimo(Cliente leitor){
        this.cliente = leitor;
        this.carrinhoEmprestimo = new Vector<>();
        this.dataI = new Date();
        this.dataF = new Date();
    }
    
    public Emprestimo(Date dataI, Date dataF, String stat, Vector<Livro> registroLivros, Cliente cliente, Vector<Livro> carrinhoEmprestimo) {
        //this.dataI = dataI;
        //this.dataF = dataF;
        this.stat = stat;
        this.registroLivros = registroLivros;
        //this.cliente = cliente;
        //this.carrinhoEmprestimo = new Vector<>();
    }


    public void setDataI(Date dataI) {
        this.dataI = dataI;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public void setRegistroLivros(Vector<Livro> registroLivros) {
        this.registroLivros = registroLivros;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    //encapsulamento
    public Date getDataI() {
        return dataI;
    }

    public Date getDataF() {
        return dataF;
    }

    public String getStat() {
        return stat;
    }

    public Vector<Livro> getRegistroLivros() {
        return registroLivros;
    }

    public Cliente getCliente() {
        return cliente;
    }
    
    public void addLivroCarrinho(Livro livroEstoque, Boolean status){
        this.carrinhoEmprestimo.add(livroEstoque);
    }
    
    public String toString(){
        String retorno = "\n**********************\nCesto de livros do cliente:\n";
        
        for(int i = 0; i < this.carrinhoEmprestimo.size(); i++){
            Livro livroTemp = this.carrinhoEmprestimo.get(i);
            
            String infLivro = livroTemp.getNome() + ", " + 
                    livroTemp.getAutor() + ", " + livroTemp.getCod() +
                    ", " + livroTemp.getEditora() + ", " + livroTemp.getAnoLanc() + "\n";
            
            retorno = retorno + infLivro + "\n";
        }
        return retorno;
    }
    
}
