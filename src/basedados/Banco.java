package basedados;

import java.util.Vector;
import modelo.Cliente;
import modelo.Livro;

public class Banco {
    
    private static Vector<Livro> livrosBanco = new Vector<>();
    private static Vector<Cliente> clientesBanco = new Vector<>();
    
    public static void iniciaBanco(){
        
        //Realizando INSERTS dos Livros no BD
        livrosBanco.add(new Livro("João e o pé de feijão", "fgjkdagkugdriuh", "Folha de S. Paulo", "Bolsonaro",1970,"111111", 2, true));
        livrosBanco.add(new Livro("O pequeno Príncipe","dkgjfglk","CineLivro","Hebert Richard",2007,"222222", 0, false));
        
        //INSERTS dos clientes
        clientesBanco.add(new Cliente("Josenilda","756456794"));
        clientesBanco.add(new Cliente("Crebeuto", "143673657", "(32) 93454-6747", "Rua Avenida Pinheiros, 59"));
    
    }
    
    //SELECT * FROM Livro
    public static Vector<Livro> selectAllLivros(){
        return livrosBanco;
    }
    
    //SELECT * FROM Cliente
    public static Vector<Cliente> selectAllClientes(){
        return clientesBanco;
    }
    
    public static Livro retornaLivroCod(String cod){
        Vector<Livro> livroEstoque = selectAllLivros();
        
        for(Livro l : livroEstoque){
            if(l.getCod().equals(cod)){
                return l;
            }
        }
        return null;
    }
    
    public static void insertLivro(Livro novoLivro){
        livrosBanco.add(novoLivro);
    }
    
    public static void insertCliente(Cliente novoCliente){
        clientesBanco.add(novoCliente);
    }
    
}
