package projetodabiblioteca;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import basedados.Banco;
import java.util.Scanner;
import java.util.Vector;
import modelo.Cliente;
import modelo.Emprestimo;
import modelo.Livro;

public class ProjetoBiblioteca {
    

    private static void consultaLivro() {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Informe o título do livro: ");
        String consulta = entrada.nextLine();
        Vector<Livro> livrosBanco = Banco.selectAllLivros();
        
        for(Livro it : livrosBanco){
            if(it.getNome().contains(consulta)){
                System.out.println(it);
            }
        }  
        
    }
    
    private static void cadastraLivro() {
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("----Cadastro de um novo livro----");
        
        System.out.println("Informe título do livro:");
        String nomeLivro = entrada.nextLine();
        
        System.out.println("Informe a sinopse do livro:");
        String sinopse = entrada.nextLine();
        
        System.out.println("Informe a editora:");
        String editora = entrada.nextLine();
        
        System.out.println("Informe o nome do autor:");
        String autor = entrada.nextLine();
        
        System.out.println("Informe o ano de lançamento:");
        int ano = entrada.nextInt();
        
        entrada.skip("\n");
        
        Livro livroBD;
        String cod;
        do{
            System.out.println("Informe o código do livro:");
            cod = entrada.nextLine();
            livroBD = Banco.retornaLivroCod(cod);
            
            if(livroBD != null){
                System.err.println("O código já está cadastrado em outro livro.");
            }
        }while(livroBD != null);
        
        
        System.out.println("Informe a quantidade de livros:");
        int quant = entrada.nextInt();
        Boolean status = null;
        
        if(quant <= 0){
            status = false;
        }else{
            status = true;
        }
        
        Livro novoLivro = new Livro(nomeLivro, sinopse, editora, autor, ano, cod, quant, status);
        
        Banco.insertLivro(novoLivro);
        System.out.println("Livro cadastrado com sucesso.");
    }
    
    public static void cadastraCliente(){
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("---Cadastrando novo cliente---");
        System.out.println("Digite:\n 1 - Cadastrar apenas por nome e CPF;\n 2 - Cadastrar  por nome, CPF, telefone e endereço");
        
        Cliente novoCliente = null;
        
        int opcao = entrada.nextInt();
        
        if(opcao == 1){
           System.out.println("Informe o nome do usuário:");
            String nome = entrada.nextLine();
        
            System.out.println("Informe o CPF:");
            String cpf = entrada.nextLine(); 
            
            novoCliente = new Cliente(nome, cpf);
            
            Banco.insertCliente(novoCliente);
            System.out.println("Cliente cadastrado com sucesso!!!\n");
        }
        
        if(opcao == 2){
            System.out.println("Informe o nome do usuário:");
            String nome = entrada.nextLine();
        
            System.out.println("Informe o CPF:");
            String cpf = entrada.nextLine();
            
            System.out.println("Informe o telefone:");
            String telefone = entrada.nextLine();
            
            System.out.println("Informe o endereço:");
            String endereco = entrada.nextLine();
            
            novoCliente = new Cliente(nome, cpf, telefone, endereco);
            
            Banco.insertCliente(novoCliente);
            System.out.println("Cliente cadastrado com sucesso!!!\n");
            
        }else{
            System.err.println("Entrada inválida!!!");
            
        }
        
    }
    
    public static void realizaEmprestimo(){
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("---Realizando novo empréstimo---");
        
        
        Boolean statusCliente = false;
        
        do{
            System.out.println("Informe o CPF do cliente: (Se deseja sair, digite 'sair')");
            String cpf = entrada.nextLine();
            Vector<Cliente> clientes = Banco.selectAllClientes();
            
            if(cpf.equalsIgnoreCase("sair")){
                System.out.println("");
                return;
            }

            for(Cliente it : clientes){
                if(it.getCpf().equals(cpf)){
                    System.out.println("Usuário encontrado.\n");
                    statusCliente = true;
                    break;
                }else{
                    System.out.println("Usuário não encontrado no sistema.\n");
                    break;
                }
            }
            
        }while(statusCliente == false);
        
        Emprestimo novoEmprestimo = new Emprestimo(null);
        
        String cod;
        Boolean status = false;
            
        for(int cont = 0; cont < 3; cont++){
            do{
                System.out.println("Verificando o status do livro.\nInforme o código do livro (Se deseja sair, digite 'sair'):");
                cod = entrada.nextLine();
                
                if(cod.equalsIgnoreCase("sair")){
                    System.out.println("");
                    return;
                }

                Vector<Livro> livros = Banco.selectAllLivros();

                for(Livro it : livros){
                    if(it.getCod().equals(cod)){
                        if(it.getStatus() == false){
                            status = false;
                            System.out.println("Livro indisponível.\n");
                        }else{
                            status = true;
                        }
                    }
                }
            }while(status == false);

            System.out.println("Livro disponível.");
            Livro livroSelec = Banco.retornaLivroCod(cod);
            novoEmprestimo.addLivroCarrinho(livroSelec, status);
            
            System.out.println(novoEmprestimo);

        }
        
        Date agora = new Date();
            
        SimpleDateFormat dd = new SimpleDateFormat();
            
        SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
            
        Calendar calendario = Calendar.getInstance(); 
        calendario.add(Calendar.DAY_OF_MONTH, 14);
        Date diaEntrega = calendario.getTime();
        
        System.out.print("Data de entrega: "+formatoData.format(diaEntrega)+"\n**********************\n\n");
        
    }
    
    public static void main(String[] args) {
        
        //carregando exemplos
        Banco.iniciaBanco();
        
        //iserir um menu para o bibliotecário
        Scanner entrada = new Scanner(System.in);
        
        int escolha;
        do{
            System.out.println("----Menu da biblioteca----\nDigite:\n 1 - Consultar um livro;\n 2 - Cadastrar um cliente;"
                    + "\n 3 - Efetuar novo empréstimo\n 4 - Efetuar devolução de livro\n 5 - Cadastrar novo livro");
            escolha = entrada.nextInt();

            switch(escolha){
                case 1:
                    consultaLivro();
                break;
                
                case 2:
                    cadastraCliente();
                    break;
                    
                case 3:
                    realizaEmprestimo();
                    break;
                    
                case 4:
                    
                    break;
                    
                case 5:
                    cadastraLivro();
                    break;

            }
        }while(escolha != 5);
    }
 
}
